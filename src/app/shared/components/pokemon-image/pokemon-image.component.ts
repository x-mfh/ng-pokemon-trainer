import { Component, Input } from "@angular/core";

@Component({
    selector: 'app-pokemon-image',
    templateUrl: './pokemon-image.component.html',
    styleUrls: ['./pokemon-image.component.css']
})

export class PokemonImageComponent {
    @Input() imageSrc: string | undefined
    @Input() imageHeight: string = "96px";

    public getFallback(event: any): void {
        event.target.src = 'assets/not-found96x96.png';
    }
}