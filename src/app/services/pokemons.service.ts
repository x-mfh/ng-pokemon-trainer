import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { PokemonApiResponse } from "../models/pokemon-api-response.model";

@Injectable({
    providedIn: 'root'
})

export class PokemonService {

    private _pokemonApiResponse: PokemonApiResponse | null = null;
    private _error: string = '';

    constructor(private readonly http: HttpClient) {
    }

    public fetchPokemons(url: string): void {
        this.http.get<PokemonApiResponse>(url)
            .subscribe(
                (response: PokemonApiResponse) => {
                    this._pokemonApiResponse = response;
                },
                (error: HttpErrorResponse) => {
                    this._error = error.message;
                }
            )
    }

    public pokemonApiReponse(): PokemonApiResponse | null {
        return this._pokemonApiResponse;
    }
}