import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { User } from "../models/user.model";

@Injectable({
    providedIn: 'root'
})

export class SessionService {

    constructor(private readonly router: Router) {
    }

    public user(): User | null {
        return this.getStorage<User>('user')
    }

    public setUser(user: User): void {
        this.setStorage<User>('user', user)
    }

    public removeUser(): void {
        localStorage.removeItem('user');
        this.router.navigateByUrl('/')
    }

    private setStorage<T>(key: string, value: T): void {
        const json: string = JSON.stringify(value);
        const encoded: string = btoa(encodeURIComponent(json));
        localStorage.setItem(key, encoded);
    }

    private getStorage<T>(key: string): T | null {
        const saved: string | null = localStorage.getItem(key);
        if (!saved) {
            return null
        }
        const decoded: string = decodeURIComponent(atob(saved));
        const data: T = JSON.parse(decoded) as T;
        return data;
    }
}