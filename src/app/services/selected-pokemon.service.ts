import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { typeWithParameters } from '@angular/compiler/src/render3/util';
import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';

@Injectable({
    providedIn: 'root'
})

export class SelectedPokemonService {

    private _pokemon: Pokemon | null = null;
    private _error: string = '';
    private _isFetching = false;


    constructor(private readonly http: HttpClient) {
    }

    public fetchPokemon(id: number): void {
        this._isFetching = true;
        this.http.get<Pokemon>('https://pokeapi.co/api/v2/pokemon/'+ id)
        .subscribe(
            (pokemon: Pokemon) => {
                this._pokemon = pokemon;
                this._isFetching = false;
            }, 
            (error: HttpErrorResponse) => {
                this._pokemon = null;
                this._error = error.message;
                this._isFetching = false;
            })
    }

    public pokemon(): Pokemon | null {
        return this._pokemon;
    }

    public isFetching(): boolean {
        return this._isFetching;
    }

    public error(): string {
        return this._error;
    }
}