import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";
import { PokemonNestedNameAndUrl } from 'src/app/models/api-utils.model'
import { getIdAndImageByUrl } from "src/app/utils/pokemon-image.util";

@Component({
    selector: 'app-pokemon-list-item',
    templateUrl: './pokemon-list-item.component.html',
    styleUrls: ['./pokemon-list-item.component.css']
})

export class PokemonListItemComponent {

    constructor(private readonly router: Router) {

    }

    @Input() _pokemon: PokemonNestedNameAndUrl | null = null

    get pokemon() {
        if(this._pokemon) {
            let { name, url } = this._pokemon;
            let { id, image } = getIdAndImageByUrl(url)
            return { name, id, image }
        }
        return null
    }

    public onPokemonClicked(): void {
        this.router.navigateByUrl('/pokemons/' + this.pokemon?.id);
    }
}