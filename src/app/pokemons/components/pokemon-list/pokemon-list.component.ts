import { Component, OnInit } from "@angular/core";
import { PokemonService } from "src/app/services/pokemons.service";

@Component({
    selector: 'app-pokemon-list',
    templateUrl: './pokemon-list.component.html',
    styleUrls: ['./pokemon-list.component.css']
})

export class PokemonListComponent implements OnInit {

    constructor(private readonly pokemonService: PokemonService) {
    }

    ngOnInit(): void {
        this.pokemonService.fetchPokemons('https://pokeapi.co/api/v2/pokemon/');
    }

    get pokemons() {
        return this.pokemonService.pokemonApiReponse()?.results
    }

    get hasNext() {
        return this.pokemonService.pokemonApiReponse()?.next
    }

    get hasPrevious() {
        return this.pokemonService.pokemonApiReponse()?.previous
    }

    public getNext() {
        const nextUrl = this.pokemonService.pokemonApiReponse()?.next;
        if (nextUrl) {
            this.pokemonService.fetchPokemons(nextUrl);
        }
    }

    public getPrev() {
        const prevUrl = this.pokemonService.pokemonApiReponse()?.previous;
        if (prevUrl) {
            this.pokemonService.fetchPokemons(prevUrl);
        }
    }
}