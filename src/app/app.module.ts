import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

import { LoginPage } from './login/pages/login/login.page';
import { ProfilePage } from './profile/pages/profile/profile.page';
import { PokemonsPage } from './pokemons/pages/pokemons/pokemons.page';
import { PokemonDetailsPage } from './pokemon-details/pages/pokemon-details/pokemon-details.page';
import { PageNotFoundPage } from './not-found/page-not-found/page-not-found.page';

import { AppComponent } from './app.component';
import { ContainerComponent } from './shared/components/container/container.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { PokemonListComponent } from './pokemons/components/pokemon-list/pokemon-list.component';
import { PokemonListItemComponent } from './pokemons/components/pokemon-list-item/pokemon-list-item.component';
import { PokemonDetailsNotFoundComponent } from './pokemon-details/components/pokemon-details-not-found/pokemon-details-not-found.component';
import { ProfilePokemonListComponent } from './profile/components/profile-pokemon-list/profile-pokemon-list.component';
import { ProfilePokemonListItemComponent } from './profile/components/profile-pokemon-list-item/profile-pokemon-list-item.component';
import { LoginFormComponent } from './login/components/login-form/login-form.component';
import { PokemonDetailsBasestatsComponent } from './pokemon-details/components/pokemon-details-basestats/pokemon-details-basestats.component';
import { PokemonDetailsProfileComponent } from './pokemon-details/components/pokemon-details-profile/pokemon-details-profile.component';
import { PokemonDetailsMovesComponent } from './pokemon-details/components/pokemon-details-moves/pokemon-details-moves.component';
import { PokemonImageComponent } from './shared/components/pokemon-image/pokemon-image.component';


@NgModule({
  declarations: [
    AppComponent,
    ContainerComponent,
    HeaderComponent,
    PokemonImageComponent,
    LoginPage,
    LoginFormComponent,
    ProfilePage,
    ProfilePokemonListComponent,
    ProfilePokemonListItemComponent,
    PokemonsPage,
    PokemonListComponent,
    PokemonListItemComponent,
    PokemonDetailsPage,
    PokemonDetailsBasestatsComponent,
    PokemonDetailsProfileComponent,
    PokemonDetailsMovesComponent,
    PokemonDetailsNotFoundComponent,
    PageNotFoundPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
