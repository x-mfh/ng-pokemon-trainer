import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./auth/auth.guard";
import { LoginPage } from "./login/pages/login/login.page";
import { PageNotFoundPage } from "./not-found/page-not-found/page-not-found.page";
import { PokemonDetailsPage } from "./pokemon-details/pages/pokemon-details/pokemon-details.page";
import { PokemonsPage } from "./pokemons/pages/pokemons/pokemons.page";
import { ProfilePage } from "./profile/pages/profile/profile.page";

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/login'
    },
    {
        path: 'login',
        component: LoginPage
    },
    {
        path: 'profile',
        component: ProfilePage,
        canActivate: [AuthGuard]
    },
    {
        path: 'pokemons',
        component: PokemonsPage,
        canActivate: [AuthGuard]
    },
    {
        path: 'pokemons/:pokemonId',
        component: PokemonDetailsPage,
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        component: PageNotFoundPage
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {}