import { Component, Input } from "@angular/core";
import { PokemonListItem } from 'src/app/models/pokemon-list-item.model';

@Component({
    selector: 'app-profile-pokemon-list',
    templateUrl: './profile-pokemon-list.component.html',
    styleUrls: ['./profile-pokemon-list.component.css']
})

export class ProfilePokemonListComponent {
    @Input() pokemons: PokemonListItem[] | undefined
}