import { Component, Input } from "@angular/core";
import { PokemonListItem } from 'src/app/models/pokemon-list-item.model';

@Component({
    selector: 'app-profile-pokemon-list-item',
    templateUrl: './profile-pokemon-list-item.component.html',
    styleUrls: ['./profile-pokemon-list-item.component.css']
})

export class ProfilePokemonListItemComponent {
    @Input() pokemon: PokemonListItem | undefined
}