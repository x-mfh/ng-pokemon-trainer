import { Component, OnInit } from "@angular/core";
import { User } from "src/app/models/user.model";
import { SessionService } from "src/app/services/session.service";

@Component({
    selector: 'app-profile-page',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.css']
})

export class ProfilePage implements OnInit {

    private _user: User | null = null;

    constructor(private readonly sessionService: SessionService) {
    }

    ngOnInit() {
        this._user = this.sessionService.user();
    }

    get user(): User | null {
        return this._user;        
    }

    public onProfileDeleteClicked(): void {
        const confirmed = confirm("Are you sure you want to delete your profile? THIS CAN'T BE UNDONE")
        if (confirmed) {
            this.sessionService.removeUser();
        }
    }
}