import { PokemonListItem } from "./pokemon-list-item.model";

export interface User {
    id: string;
    username: string;
    collectedPokemons: PokemonListItem[];
}