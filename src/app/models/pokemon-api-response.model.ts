import { PokemonNestedNameAndUrl } from "./api-utils.model";

export interface PokemonApiResponse {
    count: number;
    next: string | null;
    previous: string | null;
    results: PokemonNestedNameAndUrl[];
}