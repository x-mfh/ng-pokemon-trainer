import { PokemonNestedNameAndUrl } from "./api-utils.model";

export interface Pokemon {
    id: number;
    name: string;
    types: PokemonTypes[];
    stats: PokemonStats[];
    height: number;
    weight: number;
    abilities: PokemonAbility[];
    base_experience: number;
    moves: PokemonMove[];
}

export interface PokemonStats {
    base_stat: number;
    effort: number;
    stat: PokemonNestedNameAndUrl;
}

export interface PokemonAbility {
    ability: PokemonNestedNameAndUrl;
    is_hidden: boolean;
    slot: number;
}

export interface PokemonTypes {
    slot: number;
    type: PokemonNestedNameAndUrl;
}

export interface PokemonMove {
    move: PokemonNestedNameAndUrl;
}
