export interface PokemonNestedNameAndUrl {
    name: string;
    url: string;
}