import { Component, Input } from "@angular/core";
import { PokemonAbility } from "src/app/models/pokemon.model";

@Component({
    selector: 'app-pokemon-details-profile',
    templateUrl: './pokemon-details-profile.component.html',
    styleUrls: ['./pokemon-details-profile.component.css']
})

export class PokemonDetailsProfileComponent {
    @Input() height: Number | undefined;
    @Input() weight: Number | undefined;
    @Input() abilities: PokemonAbility[] | undefined;
    @Input() base_experience: Number | undefined;
}