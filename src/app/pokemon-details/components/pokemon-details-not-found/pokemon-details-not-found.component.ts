import { Component, Input } from "@angular/core";

@Component({
    selector: 'app-pokemon-details-not-found',
    templateUrl: './pokemon-details-not-found.component.html',
    styleUrls: ['./pokemon-details-not-found.component.css']
})

export class PokemonDetailsNotFoundComponent {
    @Input() error: string | undefined;
}