import { Component, Input } from "@angular/core";
import { PokemonStats, PokemonTypes } from "src/app/models/pokemon.model";
import { getImageByPokemonId } from "src/app/utils/pokemon-image.util";

@Component({
    selector: 'app-pokemon-details-basestats',
    templateUrl: './pokemon-details-basestats.component.html',
    styleUrls: ['./pokemon-details-basestats.component.css']
})

export class PokemonDetailsBasestatsComponent {
    @Input() id: Number | undefined;
    @Input() name: string | undefined;
    @Input() types: PokemonTypes[] | undefined;
    @Input() stats: PokemonStats[] | undefined;

    get image(): any {
        if(this.id) {
            return getImageByPokemonId(this.id);
        }
        return null
    }
}