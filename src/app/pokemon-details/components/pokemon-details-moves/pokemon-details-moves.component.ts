import { Component, Input } from "@angular/core";
import { PokemonMove } from "src/app/models/pokemon.model";

@Component({
    selector: 'app-pokemon-details-moves',
    templateUrl: './pokemon-details-moves.component.html',
    styleUrls: ['./pokemon-details-moves.component.css']
})

export class PokemonDetailsMovesComponent {
    @Input() moves: PokemonMove[] | undefined
}