import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { Pokemon } from "src/app/models/pokemon.model";
import { PokemonListItem } from "src/app/models/pokemon-list-item.model";
import { SelectedPokemonService } from "src/app/services/selected-pokemon.service";
import { SessionService } from "src/app/services/session.service";

@Component({
    selector: 'app-pokemon-detail',
    templateUrl: './pokemon-details.page.html',
    styleUrls: ['./pokemon-details.page.css']
})

export class PokemonDetailsPage implements OnInit {

    constructor(
        private readonly route: ActivatedRoute,
        private readonly selectedPokemonService: SelectedPokemonService,
        private readonly sessionService: SessionService
    ) { }

    ngOnInit() {
        const routeParams = this.route.snapshot.paramMap;
        const pokemonIdFromRoute = Number(routeParams.get('pokemonId'));
        if(isNaN(pokemonIdFromRoute)) {
            return
        }

        this.selectedPokemonService.fetchPokemon(pokemonIdFromRoute);
    }

    get isFetching(): boolean {
        return this.selectedPokemonService.isFetching();
    }
    
    get pokemon(): Pokemon | null {
        return this.selectedPokemonService.pokemon();
    }

    get error(): string {
        return this.selectedPokemonService.error();
    }

    get hasPokemon(): boolean {
        const user = this.sessionService.user();
        const fPokemon = this.formatSelectedPokemon();
        
        if (user && fPokemon) {
            if (user?.collectedPokemons.find(x => x.id === fPokemon?.id)) {
                return true;
            }
        }
    
        return false;
    }

    public onCatchClicked(): void {
        const user = this.sessionService.user();
        const fPokemon = this.formatSelectedPokemon();

        if (user && fPokemon) {
            if (!user?.collectedPokemons.find(x => x.id === fPokemon?.id)) {
                this.sessionService.setUser({
                    ...user,
                    collectedPokemons: [...user.collectedPokemons, fPokemon]
                })
            }
        }
    }

    private formatSelectedPokemon(): PokemonListItem | null {
        const pokemon = this.selectedPokemonService.pokemon();
        if (pokemon) {
            const formattedPokemon: PokemonListItem = {
                id: pokemon.id,
                name: pokemon.name,
                imageURL: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ pokemon.id }.png`
            }

            return formattedPokemon;
        }
        return null;
    }
}