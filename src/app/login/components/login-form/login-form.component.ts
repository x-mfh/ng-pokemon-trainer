import { Component, OnInit } from "@angular/core";
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/models/user.model';

@Component({
    selector: 'app-login-form',
    templateUrl: './login-form.component.html',
    styleUrls: ['./login-form.component.css']
})

export class LoginFormComponent implements OnInit {
    constructor(
        private readonly authService: AuthService,
        private readonly router: Router
        ) {
    }

    ngOnInit() {
        if (this.authService.isLoggedIn()) {
            this.router.navigateByUrl('/pokemons')
        }
    }

    public onSubmit(loginForm: NgForm): void {
        const username: string = loginForm.value.username as string;

        const user: User = {
            id: Math.random().toString(16).slice(2),
            username,
            collectedPokemons: []
        }

        this.authService.login(user);
        this.router.navigateByUrl('/pokemons')
    }
}