import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { SessionService } from '../services/session.service';

@Injectable({
    providedIn: 'root',
})

export class AuthService {

    constructor(private readonly sessionService: SessionService){
    }

    isLoggedIn(): boolean {
        if(this.sessionService.user()) {
            return true;
        }
        return false;
    }

    login(user: User): void {
        this.sessionService.setUser(user)
    }
}