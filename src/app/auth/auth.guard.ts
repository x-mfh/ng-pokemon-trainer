import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root',
})

export class AuthGuard implements CanActivate {
    constructor(
        private readonly authService: AuthService, 
        private readonly router: Router
        ) {
    }

    canActivate(): true|UrlTree {
        return this.checkLogin();
    }

    checkLogin(): true|UrlTree {
        if (this.authService.isLoggedIn()) { 
            return true; 
        }

        return this.router.parseUrl('/login');
    }
}