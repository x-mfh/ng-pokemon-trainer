export function getIdAndImageByUrl(url: string): any {
    const id = url.split( '/' ).filter( Boolean ).pop();
    return { id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`};
}

export function getImageByPokemonId(id: Number): string {
    return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`;
}